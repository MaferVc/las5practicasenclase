package com.integrador.proyecto.cuartoa;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActividadPrincipal extends AppCompatActivity {
Button btnlogin, btningresar, btnbuscar, btnParametro, btnFragmentos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);

        btnlogin = (Button)findViewById(R.id.btnlogin);
        btningresar = (Button)findViewById(R.id.btningresar);
        btnbuscar = (Button)findViewById(R.id.btnbuscar);
        btnParametro = (Button)findViewById(R.id.btnPasarParametro);
        btnFragmentos = (Button) findViewById(R.id.btnFragmentos);


        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPrincipal.this,ActividadLogin.class);
                        startActivity(intent);
            }
        });

        btningresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPrincipal.this,ActividadRegistrar.class);
                startActivity(intent);
            }
        });

        btnbuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPrincipal.this,ActividadBuscar.class);
                startActivity(intent);
            }
        });

        btnParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPrincipal.this,ActividadPasarParametro.class);
                startActivity(intent);
            }
        });

        btnFragmentos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPrincipal.this, ActividadFragmento.class);
                startActivity(intent);
            }
        });

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.opcionLogin:
                intent = new Intent(ActividadPrincipal.this,ActividadLogin.class);
                startActivity(intent);
                break;
            case R.id.opcionRegistrar:
                intent = new Intent(ActividadPrincipal.this, ActividadRegistrar.class);
                startActivity(intent);
                break;
            case R.id.dialogoLogin:

                Dialog dialogoLogin1 = new Dialog(ActividadPrincipal.this);
                dialogoLogin1.setContentView(R.layout.dlg_login);

                Button btnAutenticar2 = (Button) dialogoLogin1.findViewById(R.id.btnAutenticar2);
                final EditText cajaUsuario1 = (EditText) dialogoLogin1.findViewById(R.id.txtUser);
                final EditText cajaClave1 = (EditText) dialogoLogin1.findViewById(R.id.txtPassword);

                btnAutenticar2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(ActividadPrincipal.this, cajaUsuario1.getText().toString() + " " +cajaClave1.getText().toString(),Toast.LENGTH_LONG).show();
                    }
                });


                dialogoLogin1.show();
                break;
            case  R.id.dialogoRegistrar:
                break;


                        }
        return true;
}}
